#define _USE_MATH_DEFINES//MACIEJ DOMURAD
#include<iostream>
#include<conio.h>
#include<math.h>
using namespace std;

int main(){
	double x,y,x1,x2,y1,y2,a,b,r,delta;
	cin>>a>>b>>r;
	delta=(a*a*r*r)-(b*b)+(r*r);//r�wnanie kwadratowe ma posta� x*x(a*a+1)+2axb+b*b-r*r i z niego wyznaczona jest dane r�wnanie na delt�
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	if(delta<0){
		cout<<"Prosta nie ma punktow wspolnych z okregiem";
		_getch();
		
	}
	if(delta==0){
		x=(-2*a*b)/(2*a*a+1);
		y=a*x+b;
		cout.width(6); 
		cout<<"prosta jest styczna do okregu w punkcie: P("<<x<<","<<y<<")";
		_getch();
	}
	if(delta>0){
		x1=(-(2*a*b)+sqrt(delta))/(2*a*a+1);
		x2=(-(2*a*b)-sqrt(delta))/(2*a*a+1);
		y1=a*x1+b;
		y2=a*x2+b;
		cout<<"prosta przecina okrag w dwoch punktach: x1=";
		cout.width(6); 
		cout<<x1<<"     y1="<<y1<<endl;
		cout.width(6);
		cout<<"x2="<<x2<<"     y2="<<y2;
		_getch();  
	}
	
	return 0;
}
